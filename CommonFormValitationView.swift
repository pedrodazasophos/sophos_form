//
//  CommonFormValitationView.swift
//  Terpel
//
//  Created by Pedro Alonso Daza B on 3/01/22.
//  Copyright © 2022 Terpel. All rights reserved.
//

import UIKit

class CommonFormValitationView: UIView {

    
    var timer: Timer?
    private var continueButton = UIButton()
    var fieldsExceptiosn = [UITextFieldCustom]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponets()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    func setButtonContinue(button: UIButton) {
        self.continueButton = button
    }
    
    func initComponets() {
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
    }
    
    func isAllFiueldsNotEmpty() -> Bool {
        var isNotEmpty = true
        for view in self.subviews {
            var isException = false
            for viewException in fieldsExceptiosn {
                if viewException == view {
                    isException = true
                    break
                }
            }
            if view.isHidden {
                isException = true
            }
            if !isException{
                for viewField in view.subviews {
                if viewField is UITextField {
                        if let textField = viewField as? UITextField {
                            
                            if textField.text?.isEmpty ?? true {
                                isNotEmpty = false
                                break
                            }
                        }
                    }
                }
            }
                
            if !isNotEmpty {
                break
            }
        }
        //print("validation: \(isNotEmpty)")
        return isNotEmpty
    }
    
    @objc func fireTimer() {
        if isAllFiueldsNotEmpty() {
            continueButton.isEnabled = true
            continueButton.backgroundColor = #colorLiteral(red: 1, green: 0.8476846814, blue: 0, alpha: 1)
        } else {
            continueButton.isEnabled = false
            continueButton.backgroundColor = #colorLiteral(red: 0.6700000167, green: 0.6700000167, blue: 0.6700000167, alpha: 1)
        }
    }
    
    

}

