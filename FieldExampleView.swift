//
//  FieldExampleView.swift
//  Terpel
//
//  Created by Pedro Alonso Daza B on 3/01/22.
//  Copyright © 2022 Terpel. All rights reserved.
//

import UIKit

class FieldExampleView: UIView {

    private var isFirstCall = false
    
    // MARK: - Properties
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var commonFormValitationView: CommonFormValitationView!
    @IBOutlet weak var asesorCodeView: UITextFieldCustom!

    @IBOutlet weak var identificationNumberView: UITextFieldCustom!
    @IBOutlet weak var fullNameView: UITextFieldCustom!
    @IBOutlet weak var continueButton: CustomButton!
    
    // MARK: - Awaket NIB
    override func awakeFromNib() {
        super.awakeFromNib()
        self.autoresizesSubviews = true;
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponets()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initComponets()
    }
    
    func initComponets(){
        if !isFirstCall {
            let viewFromXib = Bundle.main.loadNibNamed("FieldExampleView", owner: self, options: nil)![0] as! UIView
            viewFromXib.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            contentView = viewFromXib
            self.addSubview(viewFromXib)
            isFirstCall = true
            
            commonFormValitationView.setButtonContinue(button: continueButton)
            
            
        }
    }
    
    
    @IBAction func continuePressed(_ sender: UIButton) {

        
    }

}
