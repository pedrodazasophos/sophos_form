//
//  UITextFieldCustom.swift
//  Terpel
//
//  Created by Pedro Alonso Daza B on 3/01/22.
//  Copyright © 2022 Terpel. All rights reserved.
//

import Foundation
import UIKit


enum TypeText {
    case text
    case alphabetical
    case numeric
    case alphaNumeric
}

class UITextFieldCustom : UIView {
    
    var textFieldInput = UITextFieldCustomInsets()
    
    private var label: UILabel = UILabel(frame: CGRect.zero)
    private let labelHeight: CGFloat = 16
    private let labelTag = Int(Int.max / 7)
    private let labelPadding: CGFloat = 3
    private let labelFontSize: CGFloat = 12
    private var isTextFieldSet = false
    
    @IBInspectable var cornerRadiusCustom: CGFloat = 0 {
        didSet {
            textFieldInput.layer.cornerRadius = cornerRadiusCustom
            textFieldInput.layer.masksToBounds = cornerRadiusCustom > 0
        }
    }
    
    @IBInspectable var lineWidth: CGFloat = 0
    @IBInspectable var lineColor: UIColor = .clear
    private var lineBorder: CAShapeLayer?
    @IBInspectable var placeholder: String?
    @IBInspectable var font: UIFont = UIFont(name: "Roboto", size: CGFloat(16))!
    @IBInspectable var secure: Bool = false
    //MARK: -Vars Validations
    let validationAlphabetical = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz"
    let validationAlphabeticalCapital = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ "
    let validationNumric = "0123456789"
    
    /*
     0 - text
     1 - alphabetical
     2 - numeric
     3 - alphaNumeric
     4 - email
     5 - decimal
     6 - amount
     7 - alphabeticCapital
     */
    @IBInspectable var inputText: Int = 0
    @IBInspectable var rangeText: Int = 20
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    
    private func initView() {
        backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        label.tag = labelTag
        label.numberOfLines = 1
        label.font = UIFontMetrics.default.scaledFont(for: font)
        label.adjustsFontForContentSizeCategory = true
        textFieldInput.translatesAutoresizingMaskIntoConstraints = false
        textFieldInput.layer.borderColor = UIColor.clear.cgColor
        textFieldInput.borderStyle = .roundedRect
        textFieldInput.addTarget(self, action: #selector(self.addFloatingLabel), for: .editingDidBegin)
        textFieldInput.addTarget(self, action: #selector(self.removeFloatingLabel), for: .editingDidEnd)

        addSubview(textFieldInput)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldInput.layer.cornerRadius = cornerRadiusCustom
        textFieldInput.placeholder = placeholder
        textFieldInput.font = font
        textFieldInput.isSecureTextEntry = secure
        label.text = placeholder
        label.textColor = lineColor
        label.font = font.withSize(labelFontSize)
        
        textFieldInput.delegate = self
        switch inputText {
        case 2:
            textFieldInput.keyboardType = .numberPad
            break
        case 6:
            textFieldInput.keyboardType = .numberPad
            break
        default:
            break
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isTextFieldSet {
            setupUITextField()
        }
        //setupUIBezierPath()
    }
}



// MARK: - Public interface and implementations

extension UITextFieldCustom {
    
    // Add a floating label to the view on becoming first responder
    @objc func addFloatingLabel() {
        label.clipsToBounds = true
        textFieldInput.layer.borderColor = lineColor.cgColor
        textFieldInput.layer.borderWidth = lineWidth
        addSubview(label)
        
        NSLayoutConstraint.activate([label.bottomAnchor.constraint(equalTo: textFieldInput.topAnchor, constant: CGFloat(-labelPadding)),
                 label.leadingAnchor.constraint(equalTo: textFieldInput.leadingAnchor, constant: textFieldInput.layoutMargins.left),
                 label.topAnchor.constraint(equalTo: self.topAnchor)])
        self.textFieldInput.placeholder = nil
        
        self.setNeedsDisplay()
    }
    
    
    @objc func removeFloatingLabel() {
        if self.textFieldInput.text?.isEmpty ?? true {
            UIView.animate(withDuration: 0.13) { [weak self] in
                if let viewWithTag = self!.viewWithTag(self!.labelTag) {
                    viewWithTag.removeFromSuperview()
                }
            }
            
            self.textFieldInput.placeholder = self.label.text
        }
        
        textFieldInput.layer.borderWidth = 0
        textFieldInput.layer.borderColor = UIColor.clear.cgColor
    }
    
    
    private func setupUITextField() {
        NSLayoutConstraint.activate([textFieldInput.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                 textFieldInput.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                 textFieldInput.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                 textFieldInput.heightAnchor.constraint(equalTo: self.heightAnchor, constant: CGFloat(-labelHeight))])
        
        isTextFieldSet = true
    }
    
    
    /*private func setupUIBezierPath() {
        lineBorder?.removeFromSuperlayer()
        let lineBorder = CAShapeLayer()
        lineBorder.lineWidth = lineWidth
        lineBorder.strokeColor = lineColor.cgColor
        lineBorder.frame = textFieldInput.bounds
        lineBorder.fillColor = nil
        
        if cornerRadiusCustom > 0 {
            lineBorder.path = UIBezierPath(roundedRect: textFieldInput.bounds, cornerRadius: cornerRadiusCustom).cgPath
        }
        else {
            lineBorder.path = UIBezierPath(rect: textFieldInput.bounds).cgPath
        }
        
        textFieldInput.layer.addSublayer(lineBorder)
        self.lineBorder = lineBorder
    }*/
    
    
    func setPlaceHolder(placeholder: String) {
        textFieldInput.placeholder = placeholder
        label.text = placeholder
    }
}



class UITextFieldCustomInsets : UITextField {
    
    @IBInspectable var leftPadding: CGFloat = 8
    @IBInspectable var gapPadding: CGFloat = 4
    
    private var textPadding: UIEdgeInsets {
        let p: CGFloat = leftPadding + gapPadding + (leftView?.frame.width ?? 0)
        return UIEdgeInsets(top: 0, left: p, bottom: 0, right: 5)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var r = super.leftViewRect(forBounds: bounds)
        r.origin.x += leftPadding
        return r
    }
}



// MARK: -Validations

extension UITextFieldCustom : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch inputText {
        case 0:
            return (range.location < rangeText)
            
        case 1:
            let set = CharacterSet(charactersIn: validationAlphabetical).inverted
            let filtered = string.components(separatedBy: set).joined(separator: "")
            print("\((range.location < rangeText)) >>>>>>>>>>>>>>>>>>>>>>   text: \(textField.text)  \(range)")

            return (string == filtered) && (range.location < rangeText)
            
        case 2:
            let set = CharacterSet(charactersIn: validationNumric).inverted
            let filtered = string.components(separatedBy: set).joined(separator: "")
            return (string == filtered) && (range.location < rangeText)
            
        case 6:
            if let textData = textField.text {
                
                var intStringValue = getIntsString(string: textData + string)
                if string == "" {
                    intStringValue = String(intStringValue.dropLast(1))
                }
                if (range.location >= rangeText) {
                    return false
                }
                let intValue = NSNumber(value: Int(intStringValue) ?? 0)
                
                let formatter = NumberFormatter()
                formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: intValue) {
                    if formatter.internationalCurrencySymbol == "COP" {
                        let substring1 = String(formattedTipAmount).dropFirst()       // "01234567"
                        let substring2 = String(substring1).dropFirst()

                        if substring2 == "0"{
                            textField.text = ""
                        }else {
                            textField.text = String(substring2)
                        }
                    } else if formatter.internationalCurrencySymbol == "¤¤"{
                        let substring1 = String(formattedTipAmount).replacingOccurrences(of: ".00", with: "")
                        let substring2 = substring1.replacingOccurrences(of: "¤", with: "")

                        if substring2 == "0"{
                            textField.text = ""
                        }else {
                            textField.text = substring2
                        }
                    } else if formatter.internationalCurrencySymbol == "USD"{
                        let substring1 = String(formattedTipAmount).replacingOccurrences(of: ".00", with: "")
                        let substring2 = substring1.replacingOccurrences(of: "$", with: "")

                        if substring2 == "0"{
                            textField.text = ""
                        }else {
                            textField.text = substring2
                        }
                    }
                    
                    
                }
                return false
            }
            
            return false
            
        case 7:
            /*let set = CharacterSet(charactersIn: validationAlphabeticalCapital).inverted
            let filtered = string.components(separatedBy: set).joined(separator: "")
            return (string == filtered) && (range.location < rangeText)*/
        
            var intStringValue = textField.text
            if string == "" {
                intStringValue = String(intStringValue!.dropLast(1))
            }
            let upperCaseString = string.uppercased()
            if let newText = textField.text{
                textField.text = newText + upperCaseString
            }
            


            return false
            
        default: break
        }
        
        return range.location < rangeText
    }
    
    
    func getIntsString(string: String) -> String{
        var numbers = ""
        
        for item in string {
            if item == "0" || item == "1" || item == "2" || item == "3" || item == "4" || item == "5" || item == "6" || item == "7" || item == "8" || item == "9" {
                numbers = numbers + "\(item)"
            }
        }
        
        return numbers
    }
}
